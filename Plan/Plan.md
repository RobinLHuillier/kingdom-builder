
**Interface**  
-> créer un mockup de l'interface général   
-> préparer plan de création de l'interface (visuel, maj des ressources, menus)  

**Dessin Construction**  
-> créer un mockup du menu de dessin de construction  
-> préparer plan de création des dessins de constructions (visuel, tool, algorithme)  

**Construction**  
-> créer un mockup des constructions  
-> préparer plan de création des constructions  

**Gestion**  
-> créer un mockup de l'écran de gestion  
-> préparer plan de création de gestion  

**Evolution**  
-> préparer plan de création de l'évolution (upgrades, recherche ? modification/ajout de gameplay ?)




**Réflexions générales:**   

Jeu js, suite de castle builders
-> construire des maisons répétitivement [construction]  
-> chaque maison débloque des gens, recevoir des ressources [idle]  
-> organiser village(s?), améliorer la vie des gens [gestion]  

-> l'utilisateur dessine la maison qu'il désire avec des requirements (matériaux: type bois/pierre, fenêtre, porte, cheminée ...)  
-> sauvegarde d'un dessin de maison par type de bâtiment  
-> possibilité de rechercher des nouvelles améliorations + des plans tous faits dans des guildes ?  

